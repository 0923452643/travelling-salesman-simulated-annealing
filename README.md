# Travelling Salesman problem

  Solving the Travelling Salesman problem by using the Simulated Annealing
   algorithm. Starting at a certain temperature, keep changing the solution
   until we find either a better route or a worse route that fall under a
   certain probability on a distribution dependent on the temperature.

  For creating a GIF a couple of scripts are available that require additional
   programs.

## Requirements:

    - Python 3.0 or higher
    - Numpy 1.13.1 for python 3

## For GIF creation:

    - Gnuplot 5.0 or higher for generating the graphics
    - Bash and ImageMagick for Linux for creating the .gif

## Syntax:

    Three options are available:
        -n or -N            : Number of Cities
        -T or --temperature : The initial temperature
        -a or --alpha       : The exponential constant for the temperature scheduler

    example:
        $ python3 src/main.py -n 29 -T 1.0 -a 0.95

### Notes:

    - For large values of the number of cities to visit, the algorithm might not get the perfect route at the end.
    - Usually having the initial temperature at 1.0 is enough, though higher values might be interesting for large values
      of the number of cities in order to unscramble the initial route.
    - The value of the exponential constant should range between 0.89 < alpha < 0.98 for maximum efficiency.
