#!/bin/bash
# Create a gif from the output file of the TSSA program
GNPLOTSCRIPT=$PWD/util/graphcity.gplt
OUTDIR="out"
if [ ! -d $OUTDIR ]; then
  echo 'Error: Output directory "'$OUTDIR'" "does not exist.'
  exit
fi
FILENAMETEMPLATE="coolingpath_"
  for FILE in $(ls -rt ./$OUTDIR/$FILENAMETEMPLATE*.txt ./$OUTDIR/final_path_*.txt ./$OUTDIR/best_path_*.txt); do
    INTITLE=`head -n1 $FILE | awk '{print $2, "=", $7, $3, "=", $8, $4,"=",  $9, $5, "=", $10}'`
    gnuplot -e "INFILE='$FILE" -e "TITLE='$INTITLE'" -e "OUTFILE='${FILE/.txt/.png}'" $GNPLOTSCRIPT;
done

convert -delay 35 -loop 0 `ls -rt ./$OUTDIR/$FILENAMETEMPLATE*.png` ./$OUTDIR/sa_ts_solution.gif
