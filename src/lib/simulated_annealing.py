# Solve the TSP using a minimax algorithm
import sys,os
if( int(sys.version[0] ) >= 3 ):
    import numpy as np
    np.random.seed()
    import lib.travelling_salesman as ts
    import copy
else:
    raise Exception("Python version must be 3.0 or higher")

def annealing(ncity, Tin, alpha):
    """
    Solve the Travelling Salesman Problem using the Simulated Annealing technique
    """
    # Temperature start and minim temperature
    T     = np.float(Tin)
    # This formula works for this particular instance and with this exact setup
    Tmin  = np.float(1.0e-4)
    TminTOL = np.ceil(500./ncity)*Tmin
    Tbest = T
    Lprev = np.float(0.0)
    pTOL  = np.float(1.0e-5)
    # Sucessful find better path and max, Max number of iterations in a fixed temperature
    js    = np.int(0)
    jsmax = np.int(92*np.log(106*(ncity-2)))
    jmax  = np.int(4.0e4)
    # Aux variables
    i     = np.int(1)
    ibest = i
    iprint= np.int(4)

    print(f' Initial set of parameters: N={ncity}, T_in={T}, Alpha={alpha}')

    # Start the Algorithm
    print("     i  T        L")
    # Generate random city state
    traveller   = ts.TravellingSalesman(ncity)
    best_traveller = copy.deepcopy(traveller)

    # Start the temperature loop
    while( T > Tmin ):
        # Print Current Iteration and temperature
        print(f"  {i:>4d}  {T:>7.5f} {traveller.L:>9.6f}",flush=True)
        # Check if the temperature is low enough that the path do not change
        if( (np.abs(traveller.L-Lprev) < pTOL) and T < TminTOL ):
            print("Termination criteria met: Tolerance")
            break
        # Update prev
        Lprev = traveller.L
        # Aux variable
        j = np.int(0)
        while( (j < jmax) or (js < jsmax) ):
            j += 1
            # Compute the distance
            traveller.L = traveller.tdist()
            # Generate a neigbour state and compute the new route distance and compute the distance difference
            new_traveller = copy.deepcopy(traveller)
            new_traveller.path = new_traveller.detour()
            new_traveller.L = new_traveller.tdist()
            if( new_traveller >= traveller ): # Boltzmann Probability Distribution
                if( np.random.uniform(0,1) < np.exp(((-1)*(new_traveller.L-traveller.L)/T)) ):
                    traveller.path = np.copy(new_traveller.path)
                    traveller.L = new_traveller.L
            else: # new_traveller.L < traveller.L
                traveller.path = np.copy(new_traveller.path)
                traveller.L =  new_traveller.L
                js += 1 # Only sum when we go down
            # Keep track of the best path found
            if( traveller <= best_traveller ):
                best_traveller.path = np.copy(traveller.path)
                best_traveller.L = traveller.L
                ibest = i
                Tbest = T
            del new_traveller
        # Print the found path
        if( i % iprint == 0 ):
            traveller.print2file(i,T,traveller.L,'coolingpath','w')
        # Exponential temperature scheduling
        T *= alpha
        i += 1

    traveller.print2file(i-1,T/alpha,traveller.L,'final_path')
    best_traveller.print2file(ibest,Tbest,best_traveller.L,'best_path')
    print(f"Best path distance: {best_traveller.L:>3.4f}")

    return 0

if( __name__ == '__main__' ):
    NameError("Library File")
