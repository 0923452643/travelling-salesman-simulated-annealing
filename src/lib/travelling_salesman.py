# Python library for the setup of the Travelling Salesman Problem
import sys,os
if( int(sys.version[0] ) >= 3 ):
    import numpy as np
    np.random.seed()
else:
    raise Exception("Python version must be 3.0 or higher")

class TravellingSalesman:
    """
    This class holds both the data and the methods for calculating the total distance
    as well as a new path for the travelling salesman problem.
    """
    def __init__(self, ncity):
        """
        Generate an initial random state of ncity cities list and compute the
        starting total distance.
        """
        self.N = np.int(ncity)
        xorig, xend, yorig, yend = map(np.float,(0.0, 1.0, 0.0, 1.0))
        self.path = np.array([(np.random.uniform(xorig,xend), np.random.uniform(yorig,yend)) for i in range(ncity)],\
            dtype=np.dtype('float,float'))
        self.L = self.tdist()
    def __eq__(self, other):
        return (self.L == other.L)
    def __le__(self, other):
        return (self.L <= other.L)
    def __ge__(self, other):
        return (self.L >= other.L)

    def tdist(self):
        """
        Computes the total distance on the current path according to the list in self.path.
        """
        return np.sum([np.sqrt((self.path[i-1][0]-self.path[i][0])**2+(self.path[i-1][1]-self.path[i][1])**2) \
            for i in range(0,self.N)], dtype=np.float)

    def detour(self):
        """
        Computes a new path at random by selecting two cities and swapping the route between them.
        """
        new_path = np.array([])
        # According some ~undefined~ reference, this is the best way for finding a new route
        i, j = np.sort(np.random.choice(list(range(self.N)),replace=False,size=2))
        return np.concatenate([self.path[:i], np.flipud(self.path[i:j+1]), self.path[j+1:]])

    def print2file(self,i,T,L,filename,mode='w'):
        """
        Prints the current path so that it displays the current step, temperature and distance
        and in a readable format for Gnuplot.
        """
        outdir = "out"
        os.makedirs(outdir,exist_ok=True)
        finalfilename = f"{filename:s}_n{self.N:03d}-T{T:2.4f}-i{i:04d}.txt"
        with open(f"./{outdir}/{finalfilename}",mode) as o:
            print(f"# i, n, T, dist = {i:>d}, {self.N:>3d}, {T:>8.5f}, {L:>8.5f}",file=o)
            print("#ID - (x, y)",file=o)
            for i,city in enumerate(self.path,1):
                print(f"{i:3d}   {city[0]:2.4f} {city[1]:2.4f}",file=o)
            # Add the first city at the end for drawing porpuses
            print(f"{i+1:3d}   {self.path[0][0]:2.4f} {self.path[0][1]:2.4f}",file=o)
        return 0

if(__name__=='__main__'):
    NameError("Library File")
