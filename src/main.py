# Main for Travelling Salesman with Simulated Annealing
import os
import sys
import argparse

if( int(sys.version[0] ) >= 3 ):
    import lib.simulated_annealing as sa
else:
    raise Exception("Python version must be 3.0 or higher")

def main():
    print("*********** Travelling Salesman Problem w/ Simulated Annealing ***********")
    parser = argparse.ArgumentParser(description='Solving the Travelling Salesman poblem using the simulated annealing heuristic technique.')
    parser.add_argument('-n','-N', type=int, dest='n', required=True, metavar='N', help='# of cities', )
    parser.add_argument('-t', '--temperature', type=float, dest='t', default=float(1.0), metavar='T', help='initial temperature')
    parser.add_argument('-a', '--alpha', type=float, dest='a', default=float(0.998), metavar='ALPHA', help='temperature scheduling factor')

    args = parser.parse_args()

    n, t, a, = args.n, args.t, args.a
    if sa.annealing(n, t, a) != 0 :
        NameError("Annealing Algorithm Error")

if(__name__=='__main__'):
    main()
